// For more information on writing tests, see
// https://scalameta.org/munit/docs/getting-started.html
/*
import Resolution.*
import stainless.collection.List

class MySuite extends munit.FunSuite {
  test("NNF: base case"){
    val x = Predicate(Named("x"), List.empty)
    assertEquals(negationNormalForm(x), x)
  }

  // The three suspects:
  val a = Function(Named("Agatha"), List.empty)
  val b = Function(Named("Butler"), List.empty) 
  val c = Function(Named("Charles"), List.empty)

  // Variables
  val x = Var(Named("x"))
  val y = Var(Named("y"))

  // Predicates
  def lives(t: Term) = Predicate(Named("lives"), List(t))
  def killed(t: Term, s: Term) = Predicate(Named("killed"), List(t, s))
  def hates(t: Term, s: Term) = Predicate(Named("hates"), List(t, s))
  def richer(t: Term, s: Term) = Predicate(Named("richer"), List(t, s))
  def eq(t: Term, s: Term) = Predicate(Named("eq"), List(t, s))

  val mansionMystery: Formula = and(List(
      Exists(x, And(lives(x), killed(x, a))),
      and(List(
        lives(a),
        lives(b),
        lives(c),
        Forall(x, Implies(lives(x), or(List(eq(x, a), eq(x, b), eq(x, c)))))
      )),
      Forall(
        x,
        Forall(
          y,
          Implies(killed(x, y), And(hates(x, y), Neg(richer(x, y))))
        )
      ),
      Forall(x, Implies(hates(a, x), Neg(hates(c, x)))),
      Forall(x, Implies(hates(a, x), Neg(eq(x, b)))),
      Forall(x, Implies(Neg(eq(x, b)), hates(a, x))),
      Forall(x, Implies(hates(b, x), Neg(richer(x, a)))),
      Forall(x, Implies(Neg(richer(x, a)), hates(b, x))),
      Forall(x, Implies(hates(a, x), hates(b, x))),
      Neg(Exists(x, Forall(y, hates(x, y)))),
      Neg(eq(a, b))
    )
  )

  val nnfMansionMystery: Formula = and(List(
      Exists(x, And(lives(x), killed(x, a))),
      and(List(
        lives(a), 
        lives(b),
        lives(c),
        Forall(x, Or(Neg(lives(x)), or(List(eq(x, a), eq(x, b), eq(x, c)))))
      )),
      Forall(
        x,
        Forall(
          y,
          Or(Neg(killed(x,y)), And(hates(x,y), Neg(richer(x,y))))
        )
      ),
      Forall(x, Or(Neg(hates(a, x)), Neg(hates(c, x)))),
      Forall(x, Or(Neg(hates(a, x)), Neg(eq(x, b)))),
      Forall(x, Or(eq(x, b), hates(a, x))),
      Forall(x, Or(Neg(hates(b, x)), Neg(richer(x, a)))),
      Forall(x, Or(richer(x, a), hates(b, x))),
      Forall(x, Or(Neg(hates(a, x)), hates(b, x))),
      Forall(x, Exists(y, Neg(hates(x, y)))),
      Neg(eq(a, b))
    )
  )

  test("Mansion NNF"){
    assertEquals(nnfMansionMystery, negationNormalForm(mansionMystery))
  }

  val x0 = Var(Synthetic(0))
  val x1 = Var(Synthetic(1))
  val x2 = Var(Synthetic(2))
  val y3 = Var(Synthetic(3))
  val x4 = Var(Synthetic(4))
  val x5 = Var(Synthetic(5))
  val x6 = Var(Synthetic(6))
  val x7 = Var(Synthetic(7))
  val x8 = Var(Synthetic(8))
  val x9 = Var(Synthetic(9))
  val x10 = Var(Synthetic(10))
  val y11 = Var(Synthetic(11))

  val syntheticMansionMystery: Formula = and(List(
      Exists(x0, And(lives(x0), killed(x0, a))),
      and(List(
        lives(a),
        lives(b),
        lives(c),
        Forall(x1, Implies(lives(x1), or(List(eq(x1, a), eq(x1, b), eq(x1, c)))))
      )),
      Forall(
        x2,
        Forall(
          y3,
          Implies(killed(x2, y3), And(hates(x2, y3), Neg(richer(x2, y3))))
        )
      ),
      Forall(x4, Implies(hates(a, x4), Neg(hates(c, x4)))),
      Forall(x5, Implies(hates(a, x5), Neg(eq(x5, b)))),
      Forall(x6, Implies(Neg(eq(x6, b)), hates(a, x6))),
      Forall(x7, Implies(hates(b, x7), Neg(richer(x7, a)))),
      Forall(x8, Implies(Neg(richer(x8, a)), hates(b, x8))),
      Forall(x9, Implies(hates(a, x9), hates(b, x9))),
      Neg(Exists(x10, Forall(y11, hates(x10, y11)))),
      Neg(eq(a, b))
    )
  )

  val nnfUniqueMansionMystery: Formula = and(List(
      Exists(x0, And(lives(x0), killed(x0, a))),
      and(List(
        lives(a), 
        lives(b),
        lives(c),
        Forall(x1, Or(Neg(lives(x1)), or(List(eq(x1, a), eq(x1, b), eq(x1, c)))))
      )),
      Forall(
        x2,
        Forall(
          y3,
          Or(Neg(killed(x2,y3)), And(hates(x2,y3), Neg(richer(x2,y3))))
        )
      ),
      Forall(x4, Or(Neg(hates(a, x4)), Neg(hates(c, x4)))),
      Forall(x5, Or(Neg(hates(a, x5)), Neg(eq(x5, b)))),
      Forall(x6, Or(eq(x6, b), hates(a, x6))),
      Forall(x7, Or(Neg(hates(b, x7)), Neg(richer(x7, a)))),
      Forall(x8, Or(richer(x8, a), hates(b, x8))),
      Forall(x9, Or(Neg(hates(a, x9)), hates(b, x9))),
      Forall(x10, Exists(y11, Neg(hates(x10, y11)))),
      Neg(eq(a, b))
    )
  )

  test("Check uniquenames"){
    assertEquals(makeVariableNamesUnique(mansionMystery)._1, syntheticMansionMystery)
    assertEquals(negationNormalForm(makeVariableNamesUnique(mansionMystery)._1), nnfUniqueMansionMystery)
  }


  val fx0  = Function(Synthetic(12), freeVariables(a).map(Var(_)))
  val fy11  = Function(Synthetic(13), List(x10))
  
  val skolemizedNNFUniqueMansionMystery: Formula = and(List(
      And(lives(fx0), killed(fx0, a)),
      and(List(
        lives(a), 
        lives(b),
        lives(c),
        Forall(x1, Or(Neg(lives(x1)), or(List(eq(x1, a), eq(x1, b), eq(x1, c)))))
      )),
      Forall(
        x2,
        Forall(
          y3,
          Or(Neg(killed(x2,y3)), And(hates(x2,y3), Neg(richer(x2,y3))))
        )
      ),
      Forall(x4, Or(Neg(hates(a, x4)), Neg(hates(c, x4)))),
      Forall(x5, Or(Neg(hates(a, x5)), Neg(eq(x5, b)))),
      Forall(x6, Or(eq(x6, b), hates(a, x6))),
      Forall(x7, Or(Neg(hates(b, x7)), Neg(richer(x7, a)))),
      Forall(x8, Or(richer(x8, a), hates(b, x8))),
      Forall(x9, Or(Neg(hates(a, x9)), hates(b, x9))),
      Forall(x10, Neg(hates(x10, fy11))),
      Neg(eq(a, b))
    )
  )

  test("Check skolemization"){
    assertEquals(skolemizationNegation(mansionMystery), skolemizedNNFUniqueMansionMystery)
  }

    val prenexSkolemizedNNFUniqueMansionMystery: Formula = and(List(
      And(lives(fx0), killed(fx0, a)),
      and(List(
        lives(a), 
        lives(b),
        lives(c),
        Or(Neg(lives(x1)), or(List(eq(x1, a), eq(x1, b), eq(x1, c))))
      )),
      Or(Neg(killed(x2,y3)), And(hates(x2,y3), Neg(richer(x2,y3)))),
      Or(Neg(hates(a, x4)), Neg(hates(c, x4))),
      Or(Neg(hates(a, x5)), Neg(eq(x5, b))),
      Or(eq(x6, b), hates(a, x6)),
      Or(Neg(hates(b, x7)), Neg(richer(x7, a))),
      Or(richer(x8, a), hates(b, x8)),
      Or(Neg(hates(a, x9)), hates(b, x9)),
      Neg(hates(x10, fy11)),
      Neg(eq(a, b))
    )
  )

  test("Check prenex"){
    assertEquals(prenexSkolemizationNegation(mansionMystery), prenexSkolemizedNNFUniqueMansionMystery)
  }  

  val clausesMansion: List[Clause] = List(
    List(Atom(lives(fx0))),
    List(Atom(killed(fx0, a))),
    List(Atom(lives(a))),
    List(Atom(lives(b))),
    List(Atom(lives(c))),
    List(Atom((Neg(lives(x1)))), Atom(eq(x1, a)), Atom(eq(x1, b)), Atom(eq(x1, c))),
    List(Atom(Neg(killed(x2, y3))), Atom(hates(x2, y3))),
    List(Atom(Neg(killed(x2, y3))), Atom(Neg(richer(x2, y3)))),
    List(Atom(Neg(hates(a, x4))), Atom(Neg(hates(c, x4)))),
    List(Atom(Neg(hates(a, x5))), Atom(Neg(eq(x5, b)))),
    List(Atom(eq(x6, b)), Atom(hates(a, x6))),
    List(Atom(Neg(hates(b, x7))), Atom(Neg(richer(x7, a)))),
    List(Atom(richer(x8, a)), Atom(hates(b, x8))),
    List(Atom(Neg(hates(a, x9))), Atom(hates(b, x9))),
    List(Atom(Neg(hates(x10, fy11)))),
    List(Atom(Neg(eq(a, b))))
  )

  test("Check to clauses"){
    printf(conjunctionPrenexSkolemizationNegation(mansionMystery).toString() + "\nhello\n")
    printf(clausesMansion.toString() + "\nhithere\n")
    assertEquals(conjunctionPrenexSkolemizationNegation(mansionMystery), clausesMansion)
  }

  test("Empty justification works") {
    assert(checkResolutionProof(List()))
  }

  def justIx(ix1 : BigInt, ix2 : BigInt):ResolutionProof = List(
    (List(Atom(lives(a)),Atom(lives(b))), Assumed),
    (List(Atom(Neg(lives(a)))), Assumed),
    (List(Atom(lives(b))), Deduced((ix1,ix2), stainless.lang.Map()))
  )

  test("check works on valid ix") {
    assert(checkResolutionProof( justIx(0,1)))
  }
  test("check fails on invalid ix") {
    assert(!checkResolutionProof( justIx(0,2)))
    assert(!checkResolutionProof( justIx(0,-1)))
    assert(!checkResolutionProof( justIx(-1,1)))
    assert(!checkResolutionProof( justIx(2,-1)))
    assert(!checkResolutionProof( justIx(1,2)))
    assert(!checkResolutionProof( justIx(-1,1)))
  }

  val mansionJustificationAxioms: ResolutionProof= 
    clausesMansion.map(c => (c, Assumed))

  //def toJustify(res: ResolutionProof, clause : Clause, ix1 : BigInt, ix2: BigInt, addedSubst: stainless.lang.Map[Identifier, Term] = stainless.lang.Map[Identifier, Term]()): ResolutionProof = 
  //  res ++ List((clause, Deduced((ix1, ix2), addedSubst)))

  def toJustifyAtom(clause : Atom, ix1 : BigInt, ix2: BigInt, addedSubst: stainless.lang.Map[Identifier, Term] = stainless.lang.Map[Identifier, Term]()): (Clause, Justification) = 
    toJustify(List(clause), ix1, ix2, addedSubst)
  def toJustify(clause : Clause, ix1 : BigInt, ix2: BigInt, addedSubst: Map[Identifier, Term] = stainless.lang.Map[Identifier, Term]()): (Clause, Justification) = 
    (clause, Deduced((ix1, ix2), addedSubst))
  val mansionReasoning : ResolutionProof = List(
    toJustifyAtom(Atom(hates(fx0, a)), 1, 6, Map[Identifier, Term](y3.name -> a, x2.name -> fx0)), //16
    toJustifyAtom(Atom(Neg(richer(fx0, a))), 1, 7, Map[Identifier, Term](y3.name -> a, x2.name -> fx0)),//17
    toJustifyAtom(Atom(hates(b, fx0)), 12, 17, Map[Identifier, Term](y3.name -> a, x2.name -> fx0, x8.name -> fx0))
    )
  test("check mansion"){
    assert(checkResolutionProof(mansionJustificationAxioms ++ mansionReasoning))
  }

  test("check invalid mansion"){
    val invalidReasoning = List(
      toJustifyAtom(Atom(hates(fx0, a)), 1, 5, Map[Identifier, Term](y3.name -> a, x2.name -> fx0)), //16
    )
    assert(!checkResolutionProof(mansionJustificationAxioms ++ invalidReasoning))
  }

  test("check valid edge cases"){
    val proof : ResolutionProof = List(
      (List(Atom(lives(a), Atom(Neg(lives(a))))), Assumed),
      (List(Atom(lives(a), Atom(Neg(lives(a))))), Deduced((0,0), Map())),
      (List(), Deduced((0,0), Map()))
    )
    assert(checkResolutionProof(proof))
  }
}
*/