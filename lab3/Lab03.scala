import lisa.automation.kernel.SimplePropositionalSolver.*
import lisa.automation.kernel.SimpleSimplifier.*


object Lab03 extends lisa.Main{

  private val x = VariableLabel("x")
  private val y = VariableLabel("y")
  private val z = VariableLabel("z")
  private val a = VariableLabel("a")
  private val Q = SchematicPredicateLabel("P", 1)
  private val H = SchematicPredicateLabel("R", 2)

  ///////////////////////
  // First Order Logic //
  ///////////////////////


  // you may need to use the following proof tactics:
  // have("_____ |- _____") by Restate
  // have("_____ |- _____") by Trivial
  // have("_____ |- _____") by Weakening     (Restate and Weakening can replace all single-premise Sequent Calculus proof steps. Try them before using Trivial.)
  // have("_____ |- _____") by LeftForall(term)(premise)
  // have("_____ |- _____") by RightForall(premise)
  // have("_____ |- _____") by LeftExists(premise)
  // have("_____ |- _____") by RightExists(term)
  // have("_____ |- _____") by InstantiateForall(term*)(premise)
  // have("_____ |- _____") by LeftOr(premise1, premise2)
  // have("_____ |- _____") by LeftImplies(premise1, premise2)
  // have("_____ |- _____") by RightIff(premise1, premise2)
  // have("_____ |- _____") by RightAnd(premise1, premise2)
  // have("_____ |- _____") by Cut(premise1, premise2)
  // andThen(applySubst(P <=> Q))      (replaces instances of P by instances of Q in the current sequent)
  // andThen(applySubst(x = y))        (replaces instances of x by instances of y in the current sequent)
  //
  // andThen("_____ |- _____") by Tactic    may be use instead of "have" and (premise). In that case, the premise is replaced by the previously proven step.
  //
  //Details about Sequent Calculus in LISA can be found here: https://github.com/epfl-lara/lisa/blob/main/Reference%20Manual/lisa.pdf

  THEOREM("Ex_All_implies_All_Ex") of "∃'x. ∀'y. 'R('x, 'y) ⊢ ∀'y. ∃'x. 'R('x, 'y)" PROOF {
    have("'R('x, 'y) |- 'R('x, 'y)") by Hypothesis
    andThen("'R('x, 'y) |- ∃'x. 'R('x, 'y)") by RightExists(x)
    andThen("∀'y. 'R('x, 'y) |- ∃'x. 'R('x, 'y)") by LeftForall(y)
    andThen("∀'y. 'R('x, 'y) |- ∀'y. ∃'x. 'R('x, 'y)") by RightForall
    andThen("(∃'x. ∀'y. 'R('x, 'y)) |- ∀'z. ∃'x. 'R('x, 'z)") by LeftExists
  } 
  show

  THEOREM("Unique_Exist_Variant") of "∃'y. ∀'x. ('P('x) ⇔ 'x='y) ⊢ ∃'y. 'P('y) ∧ (∀'x. 'P('x) ⇒ 'x='y)" PROOF {
    have("∀'x. 'P('x) <=> 'x ='y |- ∀'x. 'P('x) <=> 'x ='y") by Hypothesis
    andThen("∀'x. 'P('x) <=> 'x ='y |- 'P('y) <=> 'y = 'y") by InstantiateForall(y)
    val zla = andThen("∀'x. 'P('x) <=> 'x ='y |- 'P('y)") by Restate

    have("('P('x) ⇒ 'x='y) |- ('P('x) ⇒ 'x='y)") by Hypothesis
    andThen("('P('x) ⇒ 'x='y) ∧ ('x='y ==> 'P('x)) |- ('P('x) ⇒ 'x='y)") by LeftAnd
    andThen("∀'x. ('P('x) ==> 'x='y) ∧ ('x='y ==> 'P('x)) |- ('P('x) ⇒ 'x='y)") by LeftForall(x)
    andThen("∀'x. ('P('x) ==> 'x='y) ∧ ('x='y ==> 'P('x)) |- ∀'x. ('P('x) ⇒ 'x='y)") by RightForall
    val zlb = andThen("∀'x. 'P('x) <=> 'x='y |- ∀'x. ('P('x) ⇒ 'x='y)") by Restate

    have(" ∀'x. ('P('x) ⇔ 'x='y) ⊢ 'P('y) ∧ (∀'x. 'P('x) ⇒ 'x='y)") by RightAnd(zla, zlb)

    andThen("∀'x. ('P('x) ⇔ 'x='y) ⊢ ∃'y. 'P('y) ∧ (∀'x. 'P('x) ⇒ 'x='y)") by RightExists(y)
    andThen("∃'y. ∀'x. ('P('x) ⇔ 'x='y) ⊢ ∃'y. 'P('y) ∧ (∀'x. 'P('x) ⇒ 'x='y)") by LeftExists
  } 
  show




  ////////////////
  // Set Theory //
  ////////////////


  //This one is given as an example
  THEOREM("Subset_Reflexivity") of " ⊢ subset_of('x, 'x)" PROOF {
    val subs = have(subsetAxiom) //  ⊢ ∀'x. ∀'y. (∀'z. 'z ∊ 'x ⇔ 'z ∊ 'y) ⇔ 'x = 'y
    showCurrentProof()               //shows the current sequent calculus proof
    val r1 = andThen(() |- forall(z, in(z, x) ==> in(z, x)) <=> subset(x, x)) by InstantiateForall(x, x)    //InstantiateForall will instantiate a universally bound formula on the right of a sequent with the given terms.
    have(() |- in(z, x) ==> in(z, x)) by Restate                                                            //Restate solves automatically a class of easy proposition, including reflexivity of equality, alpha-equivalence of formulas, and some propositional logic properties
    andThen(() |- forall(z, in(z, x) ==> in(z, x))) by RightForall
    andThen(applySubst(forall(z, in(z, x) ==> in(z, x)) <=> subset(x, x)))                                  //applySubst will replace  occurences of the left-hand-side of the equivalence given in argument by the right-hand-side in the current sequent.
    Discharge(r1)                                                                                           //Discharge removes the formula proven on the right of sequent r1 from the left-hand-side of the current sequent
  }

  THEOREM("Subset_Transitivity") of "subset_of('x, 'y); subset_of('y, 'z) ⊢ subset_of('x, 'z)" PROOF {
    val subs = have(subsetAxiom)           //  ⊢ ∀'x. ∀'y. (∀'z. 'z ∊ 'x ⇔ 'z ∊ 'y) ⇔ 'x = 'y

    //expand all definitions
    have(() |- subset(x, y) <=> forall(z, in(z, x) ==> in(z, y))) by InstantiateForall(x, y)(subs)
    val expandedAnd1 = andThen(() |- (subset(x, y) ==> forall(z, in(z, x) ==> in(z, y))) /\ (forall(z, in(z, x) ==> in(z, y)) ==> subset(x, y))) by Restate
    have(() |- subset(y, z) <=> forall(a, in(a, y) ==> in(a, z))) by InstantiateForall(y, z)(subs)
    val expandedAnd2 = andThen(() |- (subset(y, z) ==> forall(a, in(a, y) ==> in(a, z))) /\ (forall(a, in(a, y) ==> in(a, z)) ==> subset(y, z))) by Restate
    have(() |- subset(x, z) <=> forall(a, in(a, x) ==> in(a, z))) by InstantiateForall(x, z)(subs)
    val expandedAnd3 = andThen(() |- (subset(x, z) ==> forall(a, in(a, x) ==> in(a, z))) /\ (forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z))) by Restate

    //prove subset(x, y) |- in(a, x) ==> in(a, y) for any a
    have((subset(x, y) ==> forall(z, in(z, x) ==> in(z, y))) |- (subset(x, y) ==> forall(z, in(z, x) ==> in(z, y)))) by Hypothesis
    andThen((subset(x, y) ==> forall(z, in(z, x) ==> in(z, y))) /\ (forall(z, in(z, x) ==> in(z, y)) ==> subset(x, y)) |- (subset(x, y) ==> forall(z, in(z, x) ==> in(z, y)))) by LeftAnd
    Discharge(expandedAnd1)
    andThen(subset(x, y) |- forall(z, in(z, x) ==> in(z, y))) by Restate
    val part1 = andThen(subset(x, y) |- in(a, x) ==> in(a, y)) by InstantiateForall(a)

    //prove subset(y, z) |- in(a, y) ==> in(a, z)
    have((subset(y, z) ==> forall(a, in(a, y) ==> in(a, z))) |- (subset(y, z) ==> forall(a, in(a, y) ==> in(a, z)))) by Hypothesis
    andThen((subset(y, z) ==> forall(a, in(a, y) ==> in(a, z))) /\ (forall(a, in(a, y) ==> in(a, z)) ==> subset(y, z)) |- (subset(y, z) ==> forall(a, in(a, y) ==> in(a, z)))) by LeftAnd
    Discharge(expandedAnd2)
    andThen(subset(y, z) |- forall(a, in(a, y) ==> in(a, z))) by Restate
    val part2 = andThen(subset(y, z) |- in(a, y) ==> in(a, z)) by InstantiateForall(a)

    //prove (subset(x, y), subset(y, z)) |- forall(a, in(a, x) ==> in(a, z))
    have((subset(x, y), subset(y, z)) |- ((in(a, x) ==> in(a, y)) /\ (in(a, y) ==> in(a, z)))) by RightAnd(part1, part2)
    andThen((subset(x, y), subset(y, z)) |- in(a, x) ==> in(a, z)) by Trivial
    val subsetImpForall = andThen((subset(x, y), subset(y, z)) |- forall(a, in(a, x) ==> in(a, z))) by RightForall

    //prove forall(a, in(a, x) ==> in(a, z)) |- subset(x, z)
    have(forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z) |- forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z)) by Hypothesis
    andThen((forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z)) /\ (subset(x, z) ==> forall(a, in(a, x) ==> in(a, z))) |- forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z)) by LeftAnd
    andThen((subset(x, z) ==> forall(a, in(a, x) ==> in(a, z))) /\ (forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z)) |- forall(a, in(a, x) ==> in(a, z)) ==> subset(x, z)) by Restate
    Discharge(expandedAnd3)
    val forallImpSubs = andThen(forall(a, in(a, x) ==> in(a, z)) |- subset(x, z)) by Restate

    //assemble the last two pieces
    have((subset(x, y), subset(y, z)) |- subset(x, z)) by Cut(subsetImpForall, forallImpSubs)
  }
  show

  THEOREM("Subset_Antisymmetry") of "subset_of('x, 'y); subset_of('y, 'x)  ⊢ 'x='y " PROOF {
    val ext = have(extensionalityAxiom)    //  ⊢ ∀'x. ∀'y. (∀'z. 'z ∊ 'x ⇔ 'z ∊ 'y) ⇔ 'x = 'y
    val subs = have(subsetAxiom)           //  ⊢ ∀'x. ∀'y. 'x ⊆ 'y ⇔ (∀'z. 'z ∊ 'x ⇒ 'z ∊ 'y)

    have(() |- subset(x, y) <=> forall(z, in(z, x) ==> in(z, y))) by InstantiateForall(x, y)(subs)
    val expandedAnd1 = andThen(() |- (subset(x, y) ==> forall(z, in(z, x) ==> in(z, y))) /\ (forall(z, in(z, x) ==> in(z, y)) ==> subset(x, y))) by Restate
    have(() |- subset(y, x) <=> forall(a, in(a, y) ==> in(a, x))) by InstantiateForall(y, x)(subs)
    val expandedAnd2 = andThen(() |- (subset(y, x) ==> forall(a, in(a, y) ==> in(a, x))) /\ (forall(a, in(a, y) ==> in(a, x)) ==> subset(y, x))) by Restate
    have(() |- forall(z, in(z, x) <=> in(z, y)) <=> (x === y)) by InstantiateForall(x, y)(ext)
    val expandedAnd3 = andThen((forall(a, in(a, x) <=> in(a, y)) ==> (x === y)) /\ ((x === y) ==> forall(a, in(a, x) <=> in(a, y)))) by Restate

    //prove subset(x, y) |- in(a, x) ==> in(a, y) for any a
    have((subset(x, y) ==> forall(z, in(z, x) ==> in(z, y))) |- (subset(x, y) ==> forall(z, in(z, x) ==> in(z, y)))) by Hypothesis
    andThen((subset(x, y) ==> forall(z, in(z, x) ==> in(z, y))) /\ (forall(z, in(z, x) ==> in(z, y)) ==> subset(x, y)) |- (subset(x, y) ==> forall(z, in(z, x) ==> in(z, y)))) by LeftAnd
    Discharge(expandedAnd1)
    andThen(subset(x, y) |- forall(z, in(z, x) ==> in(z, y))) by Restate
    val part1 = andThen(subset(x, y) |- in(a, x) ==> in(a, y)) by InstantiateForall(a)

    //prove subset(y, x) |- in(a, y) ==> in(a, x)
    have((subset(y, x) ==> forall(a, in(a, y) ==> in(a, x))) |- (subset(y, x) ==> forall(a, in(a, y) ==> in(a, x)))) by Hypothesis
    andThen((subset(y, x) ==> forall(a, in(a, y) ==> in(a, x))) /\ (forall(a, in(a, y) ==> in(a, x)) ==> subset(y, x)) |- (subset(y, x) ==> forall(a, in(a, y) ==> in(a, x)))) by LeftAnd
    Discharge(expandedAnd2)
    andThen(subset(y, x) |- forall(a, in(a, y) ==> in(a, x))) by Restate
    val part2 = andThen(subset(y, x) |- in(a, y) ==> in(a, x)) by InstantiateForall(a)

    //prove subset(y, x), subset(x, y) |- forall(a, in(a, y) <=> in(a, x))
    have((subset(x, y), subset(y, x)) |- ((in(a, x) ==> in(a, y)) /\ (in(a, y) ==> in(a, x)))) by RightAnd(part1, part2)
    andThen((subset(x, y), subset(y, x)) |- in(a, y) <=> in(a, x)) by Restate
    val subsetImpForall = andThen((subset(x, y), subset(y, x)) |- forall(a, in(a, y) <=> in(a, x))) by RightForall

    //prove forall(a, in(a, y) <=> in(a, x)) |- x = y
    have(forall(a, in(a, y) <=> in(a, x)) ==> (x === y) |- forall(a, in(a, y) <=> in(a, x)) ==> (x === y)) by Hypothesis
    andThen((forall(a, in(a, y) <=> in(a, x)) ==> (x === y)) /\ ((x === y) ==> forall(a, in(a, y) <=> in(a, x))) |- (forall(a, in(a, y) <=> in(a, x)) ==> (x === y))) by LeftAnd
    andThen((forall(a, in(a, x) <=> in(a, y)) ==> (x === y)) /\ ((x === y) ==> forall(a, in(a, x) <=> in(a, y))) |- (forall(a, in(a, y) <=> in(a, x)) ==> (x === y))) by Restate
    Discharge(expandedAnd3)
    val forallImpSubs = andThen((forall(a, in(a, y) <=> in(a, x)) |- (x === y))) by Restate

    //glue the two last pieces together 
    have((subset(x, y), subset(y, x)) |- x === y) by Cut(subsetImpForall, forallImpSubs)
  }
  show

}
