import LeftistHeap.*

import stainless.lang.*
import stainless.collection.*
import stainless.annotation.*

sealed trait LeftistHeap:

    // rank property states that the tree has more elements on
    // the left than on the right, and this remains true recursively
    def hasValidRank : Boolean = 
        this match 
            case Empty => true
            case Node(l, e, r) => l.rank >= r.rank && l.hasValidRank && r.hasValidRank
        
    def isValidHeap : Boolean =
        this match
            case Empty => true
            case Node(l, e, r) => (e == minOfHeap(this).get) && l.isValidHeap && r.isValidHeap
    
    def isValidLeftistHeap = hasValidRank && isValidHeap

    def rank : BigInt = { 
        this match
            case Empty => BigInt(0)
            case Node(left, elem, right) => left.rank + 1 + right.rank
    }.ensuring(_ >= 0)

    def findMin : Option[Int] =
        this match
            case Empty => None()
            case Node(_, elem, _) => Some(elem)
    
    def deleteMin : (Option[Int], LeftistHeap) = 
        require(this.isValidLeftistHeap)
        this match
            case Empty => (None(), Empty)
            case Node(l, e, r) => (Some(e), l.mergeWith(r))

    def mergeWith(that : LeftistHeap) : LeftistHeap =
        require(this.isValidLeftistHeap && that.isValidLeftistHeap)
        decreases(this.rank + that.rank)
        (this, that) match
            case (Empty, _) => that
            case (_, Empty) => this
            case (Node(l1, e1, r1), Node(l2, e2, r2)) => 
                if(e1 < e2) 
                    makeT(e1, l1, r1.mergeWith(that))
                else
                    makeT(e2, l2, this.mergeWith(r2))

    def insert(i : Int) : LeftistHeap = 
        require(this.isValidLeftistHeap)
        mergeWith(singleton(i))

    def remove(i : Int) : LeftistHeap = 
        require(this.isValidLeftistHeap)
        val (_, remTree) = this.removedFoundAndTree(i)
        remTree
    
    def removedFoundAndTree(i : Int) : (Boolean, LeftistHeap) = {
        require(this.isValidLeftistHeap)
        this match
            case Empty => (false, Empty)
            case Node(l, e, r) =>
                if (i == e) 
                    (true, l.mergeWith(r))
                else
                    val (leftFound, newLeft) = l.removedFoundAndTree(i)
                    if leftFound then
                        (true, makeT(e, newLeft, r))
                    else
                        val (rightFound, newRight) = r.removedFoundAndTree(i)
                        (rightFound, makeT(e, l, newRight))
    }.ensuring(res => res._2.isValidLeftistHeap)

    def contains(i : Int) : Boolean = 
        this match
            case Empty => false
            case Node(l, e, r) => 
                if (i == e) 
                    true
                else
                    l.contains(i) || r.contains(i)

    def filter(f: Int => Boolean): LeftistHeap = {
        require(this.isValidLeftistHeap)
        this match
            case Empty => Empty
            case Node(left, elem, right) =>
                val leftf = left.filter(f)
                val rightf = right.filter(f)
                if f(elem) then 
                    makeT(elem, leftf, rightf)
                else 
                    LeftistHeapVerification.minOfMergeIsMinOfMins(leftf, rightf)
                    leftf.mergeWith(rightf)
    }.ensuring( res => {
        res.isValidLeftistHeap && { 
        minOfHeap(this) match
            case None() => true 
            case Some(x) => x <= minOfHeap(res).getOrElse(x)
        }
    })

      
    def contents : Set[Int] = {
        this match
            case Empty => Set.empty
            case Node(l, e, r) => (l.contents ++ r.contents) + e
    }

private case object Empty extends LeftistHeap
private case class Node(left : LeftistHeap, elem : Int, right : LeftistHeap) extends LeftistHeap {
    require(left.rank >= right.rank && elem <= minOfHeap(left).getOrElse(elem) && elem <= minOfHeap(right).getOrElse(elem))
}
  
object LeftistHeap:
    def empty : LeftistHeap = Empty

    def singleton(i : Int) : LeftistHeap = Node(Empty, i, Empty)

    def makeT(toInsert : Int, first : LeftistHeap, second : LeftistHeap) = 
        require(first.isValidLeftistHeap && second.isValidLeftistHeap)
        require(toInsert <= minOfHeap(first).getOrElse(toInsert))
        require(toInsert <= minOfHeap(second).getOrElse(toInsert))

        if(first.rank >= second.rank)
            Node(first, toInsert, second)
        else 
            Node(second, toInsert, first)

    def min(a : Int, b : Int) : Int =
        if(a <= b) then a else b

    def minOfHeap(h : LeftistHeap) : Option[Int] = 
        h match
            case Empty => None()
            case Node(l, e, r) => 
                val lmin = minOfHeap(l).getOrElse(e)
                val rmin = minOfHeap(r).getOrElse(e)
                Some(min(min(lmin, rmin), e))

object LeftistHeapVerification:
    import LeftistHeap.*

    // ========================================
    // some miscellaneous we might need later
    // ========================================

    def subtreesAreLeftistHeaps(n : Node) = {
        require(n.isValidLeftistHeap)
        n match 
            case Node(l, _, r) => (l, r)
    }.ensuring { (l, r) => l.isValidLeftistHeap && r.isValidLeftistHeap }

    def deleteMinIsRemoveOfMin(m : LeftistHeap) = {
        require(m.isValidLeftistHeap)
        require(m.findMin.isDefined)
    }.ensuring(m.deleteMin._2 == m.remove(m.findMin.get))

    def minOfMergeIsMinOfMins(l : LeftistHeap, r : LeftistHeap) : Unit = {
        require(l.isValidLeftistHeap && r.isValidLeftistHeap)
        decreases(l.rank + r.rank)
        (l, r) match
            case (Node(l1, e1, r1), Node(l2, e2, r2)) => 
                if e1 < e2 then
                    minOfMergeIsMinOfMins(r1, r)
                else
                    minOfMergeIsMinOfMins(l, r2)                    
            case _ => ()
    }.ensuring(minOfHeap(l.mergeWith(r)) match
        case None() => minOfHeap(l) == None() && minOfHeap(r) == None()
        case Some(e) => e == min(minOfHeap(l).getOrElse(e), minOfHeap(r).getOrElse(e)) 
    )

    // ========================================
    // content preservation : makeT, then merge
    // ========================================

    def makeTPreservesContent(toInsert : Int, first : LeftistHeap, second : LeftistHeap) : Unit = {
        require(first.isValidLeftistHeap && second.isValidLeftistHeap)
        require(toInsert <= minOfHeap(first).getOrElse(toInsert))
        require(toInsert <= minOfHeap(second).getOrElse(toInsert))
    }.ensuring(makeT(toInsert, first, second).contents == makeT(toInsert, first, second).contents)

    def mergePreservesContent(h1 : LeftistHeap, h2 : LeftistHeap) : Unit = {
        require(h1.isValidLeftistHeap && h2.isValidLeftistHeap)
        decreases(h1.rank + h2.rank)
        (h1, h2) match
        case (Empty, _) => ()
        case (_, Empty) => ()
        case (Node(l1, e1, r1), Node(l2, e2, r2)) => 
            if(e1 < e2)
                mergePreservesContent(r1, h2)
                makeTPreservesContent(e1, l1, r1.mergeWith(h2))
            else 
                mergePreservesContent(h1, r2)
                makeTPreservesContent(e2, l2, h1.mergeWith(r2))
    }.ensuring(h1.mergeWith(h2).contents == h1.contents ++ h2.contents)

    //but actually, we can prove a stronger result :
    //not only do the sets stay the same, but the count
    //of elements is the same

    def count(i : Int, h : LeftistHeap) : BigInt = {
        decreases(h)
        h match {
            case Empty => BigInt(0)
            case Node(l, e, r) => {
                val here = if e == i then BigInt(1) else BigInt(0)
                here + count(i, l) + count(i, r)
            }
        }
    }.ensuring(_ >= 0)

    def makeTPreservesCount(l : LeftistHeap, e : Int, r : LeftistHeap, toCount : Int) = {
        require(l.isValidLeftistHeap && r.isValidLeftistHeap)
        require(e <= minOfHeap(l).getOrElse(e))
        require(e <= minOfHeap(r).getOrElse(e))
    }.ensuring(
        if toCount == e then
            count(toCount, makeT(e, l, r)) == count(toCount, l) + count(toCount, r) + 1
        else
            count(toCount, makeT(e, l, r)) == count(toCount, l) + count(toCount, r)
    )

    def mergePreservesCount(i : Int, thiz : LeftistHeap, that : LeftistHeap) : Unit = {
        require(thiz.isValidLeftistHeap && that.isValidLeftistHeap)
        decreases(thiz.rank + that.rank)
        (thiz, that) match {
            case (_, Empty) => ()
            case (Empty, _) => ()
            case (Node(l1, e1, r1), Node(l2, e2, r2)) => {
                if(e1 < e2) then
                    mergePreservesCount(i, r1, that)
                else
                    mergePreservesCount(i, thiz, r2)
            }
        }
    }.ensuring(_ => count(i, thiz) + count(i, that) == count(i, thiz.mergeWith(that)))

    //we can now prove more results : insert behaves as we expect 

    def insertHasCorrectCount(i : Int, toInsert : Int, h : LeftistHeap) : Unit = {
        require(h.isValidLeftistHeap)
        mergePreservesCount(i, h, singleton(toInsert))
    }.ensuring{_ => 
        if i == toInsert then 
            count(i, h.insert(toInsert)) == count(i, h) + 1 
        else 
            count(i, h.insert(toInsert)) == count(i, h)
    }

    //and so does remove :

    def removeHasCorrectCount(i : Int, toDelete : Int, h : LeftistHeap) : Unit = {
        require(h.isValidLeftistHeap)
        h match
            case Empty => ()
            case Node(l, e, r) => 
                if h.contains(i) && i == toDelete then 
                    if e == toDelete then
                        mergePreservesCount(i, l, r)
                    else 
                        assert(l.contains(toDelete) || r.contains(toDelete))
                        if(l.contains(toDelete))
                            rfatIsTrueOnContains(l, toDelete)
                            removeHasCorrectCount(i, toDelete, l)
                        else
                            rfatIsFalseOnNotContains(l, toDelete)
                            removeHasCorrectCount(i, toDelete, r)
                else
                    if e == toDelete then
                        mergePreservesCount(i, l, r)
                    else 
                        removeHasCorrectCount(i, toDelete, l)
                        removeHasCorrectCount(i, toDelete, r)                    
    }.ensuring{_ =>
        if h.contains(i) then
            if i == toDelete then
                count(i, h.remove(toDelete)) == count(i, h) - 1
            else 
                count(i, h.remove(toDelete)) == count(i, h)
        else 
            count(i, h) == count(i, h.remove(toDelete))
    }

    //along with a tiny corollary
    def deleteMinHasCorrectCount(i : Int, h : LeftistHeap) = {
        require(h.isValidLeftistHeap)
        h match
            case Empty => ()
            case Node(l, e, r) =>
                deleteMinIsRemoveOfMin(h)
                removeHasCorrectCount(i, h.findMin.get, h)
    }.ensuring{_ =>
        if h.contains(i) then
            if i == h.findMin.get then
                count(i, h.deleteMin._2) == count(i, h) - 1
            else 
                count(i, h.deleteMin._2) == count(i, h)
        else 
            count(i, h) == count(i, h.deleteMin._2)
    }

    //for that, we use the following helpers that state that h.contains(e) <=> h.removedFoundAndTree(e)._1
    //i.e. remove succeeds iff there is something to remove

    def rfatIsTrueOnContains(h : LeftistHeap, toDelete : Int) : Unit = {
        require(h.contains(toDelete) && h.isValidLeftistHeap)
        h match
            case Empty => ()
            case Node(l, e, r) => 
                assert(toDelete == e || l.contains(toDelete) || r.contains(toDelete))
                if l.contains(toDelete) then
                    rfatIsTrueOnContains(l, toDelete)
                else if r.contains(toDelete) then
                    rfatIsTrueOnContains(r, toDelete)
                else
                    ()
    }.ensuring(h.removedFoundAndTree(toDelete)._1)

    def rfatIsFalseOnNotContains(h : LeftistHeap, toDelete : Int) : Unit = {
        require(!h.contains(toDelete))
        h match
            case Empty => ()
            case Node(l, e, r) => 
                assert(toDelete != e && !l.contains(toDelete) && !r.contains(toDelete))
                rfatIsFalseOnNotContains(l, toDelete)
                rfatIsFalseOnNotContains(r, toDelete)
    }.ensuring(!h.removedFoundAndTree(toDelete)._1)

    //and we can also state the same for singleton, which is a trivial result : 
    def singletonHasCorrectCount(i : Int, toCount : Int) = {

    }.ensuring {
        if i == toCount then 
            count(toCount, singleton(i)) == 1
        else
            count(toCount, singleton(i)) == 0
    }

    def filterHasCorrectCount(h : LeftistHeap, f : Int => Boolean, i : Int) : Unit = {
        require(h.isValidLeftistHeap)
        h match
            case Empty => ()
            case Node(left, elem, right) if !f(i) => 
                filterHasCorrectCount(left, f, i)
                filterHasCorrectCount(right, f, i)
                assert(count(i, left.filter(f)) == 0)
                assert(count(i, right.filter(f)) == 0)
                mergePreservesCount(i, left.filter(f), right.filter(f))
            case Node(left, elem, right) if f(i) => 
                filterHasCorrectCount(left, f, i)
                filterHasCorrectCount(right, f, i)
                assert(count(i, left.filter(f)) == count(i, left))
                assert(count(i, right.filter(f)) == count(i, right))
                makeTPreservesCount(left.filter(f), elem, right.filter(f), i)
                if(elem == i) then
                    assert(count(i, h) == count(i, left) + count(i, right) + 1)
                    assert(count(i, h.filter(f)) == count(i, left.filter(f)) + count(i, right.filter(f)) + 1)
                else 
                    assert(count(i, h) == count(i, left) + count(i, right))
                    mergePreservesCount(i, left.filter(f), right.filter(f))
                    assert(count(i, h.filter(f)) == count(i, left.filter(f)) + count(i, right.filter(f)))
    }.ensuring{
        if !f(i) then
            count(i, h.filter(f)) == 0
        else
            count(i, h.filter(f)) == count(i, h)
    }
    // thus, all methods that output a leftist tree do so while respecting the
    // reasonable specification for the count method. In other words, the methods
    // do not spuriously create or delete nodes

    // now, let's see whether they also respect both core properties of heaps

    // =================================
    // rank preservation property
    // =================================

    def makeTHasValidRank(toInsert : Int, first : LeftistHeap, second : LeftistHeap) : Unit = {
        require(first.isValidLeftistHeap && second.isValidLeftistHeap)
        require(toInsert <= minOfHeap(first).getOrElse(toInsert))
        require(toInsert <= minOfHeap(second).getOrElse(toInsert))
    }.ensuring(makeT(toInsert, first, second).hasValidRank)

    def mergeHasValidRank(h1 : LeftistHeap, h2 : LeftistHeap) : Unit = {
        require(h1.isValidLeftistHeap && h2.isValidLeftistHeap)
        decreases(h1.rank + h2.rank)
        (h1, h2) match
            case (Empty, _) => ()
            case (_, Empty) => ()
            case (Node(l1, e1, r1), Node(l2, e2, r2)) => 
                if(e1 < e2) then
                    mergeHasValidRank(r1, h2)
                    makeTHasValidRank(e1, l1, r1.mergeWith(h2))
                else 
                    mergeHasValidRank(h1, r2)
                    makeTHasValidRank(e2, l2, h1.mergeWith(r2))
    }.ensuring(h1.mergeWith(h2).hasValidRank)

    def removeHasValidRank(i : Int, h : LeftistHeap) : Unit = {
        require(h.isValidLeftistHeap)
        decreases(h.rank)
        h match
            case Empty => ()
            case Node(l, e, r) => 
                if (i == e) 
                    mergeHasValidRank(l, r)
                else
                    removeHasValidRank(i, l)
                    removeHasValidRank(i, r)
    }.ensuring(h.remove(i).hasValidRank)

    def deleteMinHasValidRank(h : LeftistHeap) = {
        require(h.isValidLeftistHeap)
        if(h.findMin.isDefined)
            deleteMinIsRemoveOfMin(h)
            removeHasValidRank(h.findMin.get, h)
    }.ensuring(h.deleteMin._2.hasValidRank)

    def insertHasValidRank(h : LeftistHeap, i : Int) = {
        require(h.isValidLeftistHeap)
        mergeHasValidRank(h, singleton(i))
    }.ensuring(h.insert(i).hasValidRank)

    def singletonHasValidRank(i : Int) = {

    }.ensuring(singleton(i).hasValidRank)

    def filterHasValidRank(f: Int => Boolean, h : LeftistHeap) : Unit = { 
        //since the recursion of filter proves filter is a LH, this becomes trivial
        require(h.isValidLeftistHeap) 
    }.ensuring(h.filter(f).hasValidRank)

    // =================================
    // heap preservation property
    // =================================

    def makeTIsValidHeap(first : LeftistHeap, toInsert : Int, second : LeftistHeap) = {
        require(first.isValidLeftistHeap && second.isValidLeftistHeap)
        require(toInsert <= minOfHeap(first).getOrElse(toInsert))
        require(toInsert <= minOfHeap(second).getOrElse(toInsert))
    }.ensuring(makeT(toInsert, first, second).isValidHeap)

    def mergeIsValidHeap(h1 : LeftistHeap, h2 : LeftistHeap) : Unit = {
        require(h1.isValidLeftistHeap && h2.isValidLeftistHeap)
        decreases(h1.rank + h2.rank)
        (h1, h2) match
            case (Empty, _) => ()
            case (_, Empty) => ()
            case (Node(l1, e1, r1), Node(l2, e2, r2)) => 
                if(e1 < e2)
                    mergeIsValidHeap(r1, h2)
                else 
                    mergeIsValidHeap(h1, r2)
    }.ensuring(h1.mergeWith(h2).isValidHeap)

    def removeIsValidHeap(i : Int, h : LeftistHeap) : Unit = {
        require(h.isValidLeftistHeap)
        decreases(h.rank)
        if (!h.contains(i))
            removeUnchangedIfNotContained(i, h)
        else
            h match
                case Empty => ()
                case Node(l, e, r) =>
                    if (i == e)
                        mergeIsValidHeap(l, r)
                    else if l.contains(i) then
                        rfatIsTrueOnContains(l, i)
                        removeIsValidHeap(i, l)
                    else
                        assert(r.contains(i))
                        rfatIsFalseOnNotContains(l, i)
                        removeIsValidHeap(i, r)
    }.ensuring(h.remove(i).isValidHeap)

    def removeUnchangedIfNotContained(i : Int, h : LeftistHeap) : Unit = {
        require(h.hasValidRank && !h.contains(i))
        decreases(h.rank)
        h match
            case Empty => ()
            case Node(l, e, r) => 
                assert(i != e)
                assert(!l.contains(i) && !r.contains(i))
                removeUnchangedIfNotContained(i, l)
                removeUnchangedIfNotContained(i, r)
    }.ensuring(h == h.remove(i))

    def deleteMinIsValidHeap(h : LeftistHeap) = {
        require(h.isValidLeftistHeap)
        if(h.findMin.isDefined)
            deleteMinIsRemoveOfMin(h)
            removeIsValidHeap(h.findMin.get, h)
    }.ensuring(h.deleteMin._2.isValidHeap)

    def insertIsValidHeap(h : LeftistHeap, i : Int) = {
        require(h.isValidLeftistHeap)
        mergeIsValidHeap(h, singleton(i))
    }.ensuring(h.insert(i).isValidHeap)

    def singletonIsValidHeap(i : Int) = {

    }.ensuring(singleton(i).isValidHeap)

    def filterIsValidHeap(f: Int => Boolean, h : LeftistHeap) : Unit = { 
        //since the recursion of filter proves filter is a LH, this becomes trivial
        require(h.isValidLeftistHeap) 
    }.ensuring(h.filter(f).isValidHeap)


    // =========================================
    // conclusion : everything is a leftist heap
    // =========================================

    def deleteMinIsALeftistHeap(h : LeftistHeap) = {
        require(h.isValidLeftistHeap)
        deleteMinIsValidHeap(h)
        deleteMinHasValidRank(h)
    }.ensuring(h.deleteMin._2.isValidLeftistHeap)

    def makeTIsALeftistHeap(first : LeftistHeap, second : LeftistHeap, elem : Int) = {
        require(first.isValidLeftistHeap && second.isValidLeftistHeap)
        require(elem <= minOfHeap(first).getOrElse(elem))
        require(elem <= minOfHeap(second).getOrElse(elem))
    }.ensuring(makeT(elem, first, second).isValidLeftistHeap)

    def mergeWithIsALeftistHeap(h1 : LeftistHeap, h2 : LeftistHeap) = {
        require(h1.isValidLeftistHeap && h2.isValidLeftistHeap)
        mergeHasValidRank(h1, h2)
        mergeIsValidHeap(h1, h2)
    }.ensuring(h1.mergeWith(h2).isValidLeftistHeap)

    def insertIsALeftistHeap(h : LeftistHeap, i : Int) = {
        require(h.isValidLeftistHeap)
        insertHasValidRank(h, i)
        insertIsValidHeap(h, i)
    }.ensuring(h.insert(i).isValidLeftistHeap)

    def removeIsALeftistHeap(h : LeftistHeap, i : Int) = {
        require(h.isValidLeftistHeap)
        removeHasValidRank(i, h)
        removeIsValidHeap(i, h)
    }

    def emptyIsALeftistHeap = {

    }.ensuring(empty.isValidLeftistHeap)

    def singletonIsALeftistHeap(i : Int) = {
        singletonHasValidRank(i)
        singletonIsValidHeap(i)
    }.ensuring(singleton(i).isValidLeftistHeap)

    def filterIsAValidLeftistHeap(h : LeftistHeap, f : Int => Boolean) = {
        require(h.isValidLeftistHeap)
    }.ensuring(h.filter(f).isValidLeftistHeap)
