theory ProgramsAreRelationsLab
  imports Main

begin
section ‹ Start by writing your solution to problem 2.10 of
http://concrete-semantics.org/concrete-semantics.pdf›

datatype tree0 = Leaf | Node "tree0" "tree0"

fun nodes :: "tree0 ⇒ nat" where
"nodes Leaf = 1" |
"nodes (Node a b) = 1 + (nodes a) + (nodes b)"

  
fun explode :: "nat ⇒ tree0 ⇒ tree0" where
"explode 0 t = t" |
"explode (Suc n) t = explode n (Node t t )"

lemma "nodes (explode n t ) = (2^n - 1) + (2^n * nodes t)"
  apply (induction n arbitrary : t)
  apply(simp)
  apply(simp add: algebra_simps)
done


section ‹ See e.g. src/HOL/Relations.thy for properties of relations

type_synonym 'a rel = "('a * 'a) set"

Basic definitions of sets are in src/HOL/Set.thy
›

section ‹Relation composition and identity›

thm "Id_def"

lemma "Id_on P = {(x,x)|x. x ∈ P}" by auto

lemma "r O (s O t) = (r O s) O t" by auto

lemma "r O Id = r" by auto

lemma "Id O r = r" by auto

lemma ‹(x,x) ∈ r^*› by auto

section ‹Programs as relations. Example with two integers›

datatype state = S (x_in: int) (y_in: int)

type_synonym command = "state rel"

definition incX :: command where
  "incX = {(S x y,S (x+1) y)|x y. True}"

definition incXby :: "int ⇒ command" where
  "incXby k = {(S x y,S (x+k) y)|x y. True}"

lemma "incX O incX = incXby 2"
  by (auto simp add: incX_def incXby_def)

definition incYby :: "int ⇒ command" where
  "incYby k = {(S x y,S x (y + k))|x y. True}"
  
lemma xySwap: "incXby dx  O  incYby dy  =  incYby dy  O  incXby dx "
  apply (simp add : incXby_def incYby_def, auto)
done
  

lemma doubleInc: "incXby dx1  O  incXby dx2  =  incXby (dx1 + dx2)"
  apply (simp add : incXby_def incYby_def, auto)
done

type_synonym condition = "state set"

definition AssumeC :: "condition ⇒ command" where
  "AssumeC cond = Id_on cond"

definition HavocX :: command where
  "HavocX = {(S x y, S x' y)| x x' y. True}"

definition HavocY :: command where
  "HavocY = {(S x y, S x y')| x y y'. True}"

definition IfC :: "condition ⇒ command ⇒ command ⇒ command" where
  "IfC cond s1 s2 = (AssumeC cond O s1) ∪ (AssumeC (- cond) O s2)"

lemma ifC_alt: 
  "IfC cond s1 s2 = {(s,s')|s s'. (s ∈ cond ⟶ (s,s')∈s1) ∧
                                  (s ∉ cond ⟶ (s,s')∈s2)}"
  apply (simp add : IfC_def AssumeC_def, auto)
done

definition WhileC :: "condition ⇒ command ⇒ command" where
  "WhileC b c = (AssumeC b O c)^* O AssumeC (- b)"

definition assignX :: "(state ⇒ int) ⇒ command" where
  "assignX f = {(s,s')|s s'. s' = S (f s) (y_in s)}"

lemma "assignX (λ s. x_in s + k) = incXby k"
  apply (simp add : assignX_def incXby_def, auto)
done

section ‹Hoare Triple›

definition triple :: "'a set ⇒ 'a rel ⇒ 'a set ⇒ bool" ("⊨ ({(1_)}/ (_)/ {(1_)})" 50)
  where
  "triple P r Q = (∀ x x'. (x ∈ P ∧ (x,x') ∈ r ⟶ x' ∈ Q))"

lemma "⊨ {{s. x_in s > 0}} incX {{s. x_in s > 0}}"
  apply(simp add : incX_def triple_def, auto)
done

lemma "k > 0 ⟹
      ⊨ {{s. x_in s ≥ 0 ∧ y_in s ≥ 0}} 
           (incX O (incYby k))
         {{s. x_in s > 0 ∧ y_in s > 0}}" 
  apply(simp add : incX_def triple_def incYby_def, auto)
done

lemma "⟦ ⊨ {P} s1 {Q} ; ⊨ {Q} s2 {R} ⟧ ⟹ 
             ⊨ {P} (s1 O s2) {R}" 
  apply(simp add: triple_def, auto)
done

lemma "⟦ ⊨ {P} c {Q} ; Q ⊆ Q' ⟧ ⟹ ⊨ {P} c {Q'}" 
  apply(simp add: triple_def, auto)
done

lemma "⟦P' ⊆ P ; ⊨ {P} c {Q}⟧ ⟹ ⊨ {P'} c {Q}" 
  apply(simp add: triple_def, auto)
done

section ‹Strongest postcondition (relation image)›

definition sp :: ‹'a set ⇒ 'a rel ⇒ 'a set› where
  ‹sp P r = {x'. ∃ x ∈ P. (x,x') ∈ r}›

lemma spPointWise: "sp P r = ⋃ {sp {s} r|s. s ∈ P}"
  apply(simp add: sp_def, auto)
done

lemma "Range {(x,y)|x y. P x y} = {y. ∃ x. P x y}"
  apply(simp add: Range_def, auto)
done

lemma sp_via_range2: "Range ((Id_on P) O r) = sp P r"
proof -
  have "Range ((Id_on P) O r)
                 = Range ({(x,x)|x. x ∈ P} O r)" 
    by auto
  also have "... = Range ({(x,x)|x. x ∈ P} O {(x,y)|x y. (x,y) ∈ r})"
    by auto
  also have "... = Range ({(x,y)|x y. x ∈ P ∧ (x,y) ∈ r})" by auto 
  also have "... = {y. ∃ x. x ∈ P ∧ (x,y) ∈ r}" by auto 
  also have "... = sp P r" by (simp add : sp_def Range_def, auto)
  finally show ?thesis .
qed

thm sym

lemma sp_via_range: "sp P r = Range ((Id_on P) O r)"
  apply(simp add: Range_def sp_def, auto)
done

lemma "sp (sp P r1) r2 = sp P (r1 O r2)" 
  apply(simp add: sp_def, auto)
done

section ‹Weakest precondition›

definition wp :: "'a rel ⇒ 'a set ⇒ 'a set" where
  "wp r Q = {x. ∀ x'. ((x,x') ∈ r ⟶ x' ∈ Q)}"

section ‹Properties Connecting triple, wp, and sp›

lemma tripleSp: "(⊨ {P} r {Q}) = (sp P r ⊆ Q)" 
 apply(simp add: sp_def triple_def, auto)
done

lemma tripleWp: "(⊨ {P} r {Q}) = (P ⊆ wp r Q)"
 apply(simp add: wp_def triple_def, auto)
done

lemma sp_post: "⊨ {P} r {sp P r}" 
  apply(simp add: sp_def triple_def, auto)
done

lemma sp_Strongest: "⊨ {P} r {Q} ⟹ sp P r ⊆ Q" 
  apply(simp add: sp_def triple_def, auto)
done

lemma wp_pre: "⊨ {wp r Q} r {Q}"
  apply(simp add: wp_def triple_def)
done

lemma wp_Weakest: "⊨ {P} r {Q} ⟹ P ⊆ wp r Q" 
  apply(simp add: wp_def triple_def, auto)
done

lemma wp_sp_dual: "- wp r Q = sp (- Q) (r^-1)" 
 apply(simp add: sp_def wp_def triple_def, auto)
done

lemma wpPointWise: "wp r Q = {s. sp {s} r ⊆ Q}" 
  apply(simp add: sp_def wp_def triple_def, auto)
done

end
